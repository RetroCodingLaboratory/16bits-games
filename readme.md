Three small .com arcade games under 1k. 

These games were made in assembly as experiments.

Klingon (882 bytes) is a version of Invaders. The commands are Ctrl for left, 
Alt for right and Shift to launch the missiles. 

Bird80 (634 bytes) is a version of Flappy Bird. Press any key flap, Esc to exit.

Meteor (612 bytes). 
The goal is to collect the green samples and avoid the asteroids. 
Hit any key to start. By pressing any key we change the direction of the spaceship.
To exit press the Esc key.

